DROP TABLE IF EXISTS weather;
DROP TABLE IF EXISTS colleagues ;
CREATE TABLE colleagues
(
  colleague_id SERIAL NOT NULL,
  full_name character varying(125) NOT NULL,
  position character varying(64) ,
  birthday_date date,
  city character varying(125),
  CONSTRAINT colleagues_pk PRIMARY KEY (colleague_id)
);

CREATE TABLE weather
(
  weather_id SERIAL NOT NULL,
  temperature integer,
  weather_status character varying(64),
  update_date timestamp without time zone,
  update_status boolean,
  colleague_id integer NOT NULL,
  CONSTRAINT weather_pkey PRIMARY KEY (weather_id),
  CONSTRAINT colleague_id FOREIGN KEY (colleague_id)
    REFERENCES colleagues (colleague_id) MATCH SIMPLE
    ON DELETE CASCADE
);
