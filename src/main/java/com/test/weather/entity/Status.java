package com.test.weather.entity;

import lombok.Getter;


public enum Status {

    SUCCESS(true),
    LOOSE(false);

    @Getter
    private boolean id;

    Status(boolean id) {
        this.id = id;
    }

    public static Status getStatusByBool(boolean status) {
        if(status){
            return Status.SUCCESS;
        } else {
            return Status.LOOSE;
        }

    }
}
