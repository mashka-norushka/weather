package com.test.weather.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Colleague {

    private long id;

    private String fullName;

    private String position;

    private Date birthdayDate;

    private String city;

    private Weather weather;
}
