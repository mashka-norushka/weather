package com.test.weather.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Weather {

    private long id;

    private int temperature;

    private String weatherStatus;

    private Date updateDate;

    @JsonIgnore
    private boolean updateStatus;
}
