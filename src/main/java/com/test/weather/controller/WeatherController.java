package com.test.weather.controller;

import com.test.weather.client.WeatherClient;
import com.test.weather.dto.WeatherResponse;
import com.test.weather.dto.converters.WeatherResponseToWeather;
import com.test.weather.entity.Weather;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherController {

    private WeatherClient client;

    public WeatherController(WeatherClient client) {
        this.client = client;
    }

    @GetMapping("/api/weather/{city}")
    public ResponseEntity<Weather> getWeatherBySity(@PathVariable String city) {
        WeatherResponse response = client.getWeatherByCity(city);
        return new ResponseEntity<>(WeatherResponseToWeather.convert(response), HttpStatus.OK);
    }
}
