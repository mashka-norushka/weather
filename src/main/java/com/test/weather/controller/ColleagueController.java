package com.test.weather.controller;

import com.test.weather.entity.Colleague;
import com.test.weather.service.ColleagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/colleague", produces = "application/json")
public class ColleagueController {

    private ColleagueService service;

    @Autowired
    public ColleagueController(ColleagueService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Colleague> getColleagueById(@PathVariable Long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @GetMapping (params = {"pageNum", "limit"})
    public ResponseEntity<List<Colleague>> getPaginated(@RequestParam("pageNum") int pageNum,
                                                        @RequestParam("limit") int limit) {
        return new ResponseEntity<>(service.getPaginated(pageNum, limit), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Colleague>> getAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Colleague> save(@RequestBody Colleague colleague) {
        return new ResponseEntity<>(service.save(colleague), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Colleague> update(@RequestBody Colleague colleague) {
        return new ResponseEntity<>(service.update(colleague), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Colleague> deleteById(@PathVariable Long id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping
    public void deleteAll() {
        service.deleteAll();
    }
}
