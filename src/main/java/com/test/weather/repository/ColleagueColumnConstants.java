package com.test.weather.repository;

public interface ColleagueColumnConstants {

    String ID = "colleague_id";
    String FULL_NAME = "full_name";
    String POSITION = "position";
    String BIRTHDAY_DATE = "birthday_date";
    String CITY = "city";
}
