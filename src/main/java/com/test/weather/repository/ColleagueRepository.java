package com.test.weather.repository;

import com.test.weather.entity.Colleague;
import com.test.weather.repository.mappers.ColleagueRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ColleagueRepository implements ColleagueColumnConstants {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ColleagueRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Colleague save(Colleague colleagueMeta) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("colleagues")
                .usingGeneratedKeyColumns("colleague_id");
        Map<String, Object> params = new HashMap<>();
        params.put(FULL_NAME, colleagueMeta.getFullName());
        params.put(POSITION, colleagueMeta.getPosition());
        params.put(BIRTHDAY_DATE, colleagueMeta.getBirthdayDate());
        params.put(CITY, colleagueMeta.getCity());

        long id = simpleJdbcInsert.executeAndReturnKey(params).longValue();

        return jdbcTemplate.queryForObject("select * from colleagues where colleague_id = ?",
                new Object[]{id},
                new ColleagueRowMapper()
        );
    }

    public Colleague update(Colleague colleague) {
        jdbcTemplate.update("update colleagues set " +
                        "full_name = ?, " +
                        "position = ?, " +
                        "birthday_date = ?, " +
                        "city = ? " +
                        "where colleague_id = ?",
                colleague.getFullName(),
                colleague.getPosition(),
                colleague.getBirthdayDate(),
                colleague.getCity(),
                colleague.getId());
        return jdbcTemplate.queryForObject("select * from colleagues where colleague_id = ?",
                new Object[]{colleague.getId()},
                new ColleagueRowMapper()
        );
    }

    public Optional<Colleague> findById(Long id) {
        Optional<Colleague> colleague;
        colleague = Optional.ofNullable(
                jdbcTemplate.queryForObject(
                        "select * from colleagues where colleague_id = ?",
                        new Object[]{id},
                        new ColleagueRowMapper()
                )
        );
        return colleague;
    }

    //PageNumber start from null
    public List<Colleague> getPaginated(int pageNumber, int limit) {
        long count = count();
        List<Colleague> paginatedList;
        if (count / limit <= pageNumber + 1) {
            paginatedList = jdbcTemplate.query("select * from colleague " +
                            "ORDER BY full_name " +
                            "LIMIT ? " +
                            "OFFSET ?",
                    new Object[]{limit, pageNumber * limit},
                    new ColleagueRowMapper()
            );
        } else {
            throw new NoSuchElementException("page not fond: the number of elements is small");
        }
        return paginatedList;
    }

    public List<Colleague> getAll() {
        List<Colleague> colleagues = jdbcTemplate.query("select * from colleagues " +
                        "ORDER BY full_name ",
                new ColleagueRowMapper()
        );
        return colleagues;
    }

    public void deleteById(Long id) {
        jdbcTemplate.update("delete from colleagues where colleague_id = ?", id);
    }

    public void deleteAll() {
        jdbcTemplate.execute("delete from colleagues where colleague_id > 0");
    }

    private long count() {
        return jdbcTemplate.queryForObject("select count(*) from colleagues", Long.class);
    }
}
