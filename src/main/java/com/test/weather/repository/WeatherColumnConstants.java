package com.test.weather.repository;

public interface WeatherColumnConstants {

    String ID = "weather_id";
    String TEMPERATURE = "temperature";
    String WEATHER_STATUS = "weather_status";
    String UPDATE_DATE = "update_date";
    String UPDATE_STATUS = "update_status";
    String COLLEAGUE_ID = "colleague_id";
}
