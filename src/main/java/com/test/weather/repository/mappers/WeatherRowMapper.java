package com.test.weather.repository.mappers;

import com.test.weather.entity.Weather;
import com.test.weather.repository.WeatherColumnConstants;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WeatherRowMapper implements RowMapper<Weather>, WeatherColumnConstants {
    @Override
    public Weather mapRow(ResultSet resultSet, int i) throws SQLException {
        return Weather.builder()
                .id(resultSet.getLong(ID))
                .temperature(resultSet.getInt(TEMPERATURE))
                .weatherStatus(resultSet.getString(WEATHER_STATUS))
                .updateDate(resultSet.getDate(UPDATE_DATE))
                .updateStatus(resultSet.getBoolean(UPDATE_STATUS))
                .build();
    }
}
