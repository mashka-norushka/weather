package com.test.weather.repository.mappers;

import com.test.weather.entity.Colleague;
import com.test.weather.repository.ColleagueColumnConstants;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ColleagueRowMapper implements RowMapper<Colleague>, ColleagueColumnConstants {

    @Override
    public Colleague mapRow(ResultSet resultSet, int i) throws SQLException {
        return Colleague.builder()
                .id(resultSet.getInt(ID))
                .fullName(resultSet.getString(FULL_NAME))
                .position(resultSet.getString(POSITION))
                .birthdayDate(resultSet.getDate(BIRTHDAY_DATE))
                .city(resultSet.getString(CITY))
                .build();
    }
}
