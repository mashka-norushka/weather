package com.test.weather.repository.mappers;

import com.test.weather.dto.WeatherRequest;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WeatherRequestRowMapper implements RowMapper<WeatherRequest> {
    @Override
    public WeatherRequest mapRow(ResultSet resultSet, int i) throws SQLException {
        return new WeatherRequest(resultSet.getLong("weather_id"),
                resultSet.getString("city"));
    }
}
