package com.test.weather.repository;

import com.test.weather.dto.WeatherRequest;
import com.test.weather.entity.Weather;
import com.test.weather.repository.mappers.WeatherRequestRowMapper;
import com.test.weather.repository.mappers.WeatherRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class WeatherRepository implements WeatherColumnConstants {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public WeatherRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Weather create(long colleagueId, Weather weather) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("weather")
                .usingGeneratedKeyColumns(ID);
        Map<String, Object> params = constructParams(colleagueId, weather);
        long id = simpleJdbcInsert.executeAndReturnKey(params).longValue();
        return jdbcTemplate.queryForObject("select * from weather where weather_id = ?",
                new Object[]{id},
                new WeatherRowMapper()
        );
    }

    public List<WeatherRequest> getAllSuccess() {
        String sql = "select weather.weather_id, " +
                "colleagues.city " +
                "from weather, colleagues " +
                "where weather.colleague_id = colleagues.colleague_id and weather.update_status = true";
        return jdbcTemplate.query(sql, new WeatherRequestRowMapper());
    }

    public List<WeatherRequest> getAllLoose() {
        String sql = "select weather.weather_id, " +
                "colleagues.city " +
                "from weather, colleagues " +
                "where weather.colleague_id = colleagues.colleague_id and weather.update_status = false";
        return jdbcTemplate.query(sql, new WeatherRequestRowMapper());
    }

    public void updateAll(List<Weather> weathers) {
        jdbcTemplate.batchUpdate("update weather set " +
                        "temperature = ?, " +
                        "weather_status = ?, " +
                        "update_date = ?, " +
                        "update_status = ? " +
                        "where weather_id = ?",
                new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(PreparedStatement preparedStatement, int i)
                            throws SQLException {
                        preparedStatement.setInt(1, weathers.get(i).getTemperature());
                        preparedStatement.setString(2, weathers.get(i).getWeatherStatus());
                        preparedStatement.setDate(3, new Date(weathers.get(i).getUpdateDate().getTime()));
                        preparedStatement.setBoolean(4, weathers.get(i).isUpdateStatus());
                        preparedStatement.setLong(5, weathers.get(i).getId());
                    }

                    @Override
                    public int getBatchSize() {
                        return weathers.size();
                    }
                });
    }

    public Weather getWeatherByColId(long colleagueId) {
        return jdbcTemplate.queryForObject("select * from weather where colleague_id = ?",
                new Object[]{colleagueId},
                new WeatherRowMapper()
        );
    }

    public void updateDateAndStatus(Weather weather) {
        jdbcTemplate.update("update weather set " +
                        "update_date = ?, " +
                        "update_status = ? " +
                        "where weather_id = ?",
                weather.getUpdateDate(),
                weather.isUpdateStatus(),
                weather.getId());
    }

    private Map<String, Object> constructParams(long colleagueId, Weather weather) {
        Map<String, Object> params = new HashMap<>();
        params.put(TEMPERATURE, weather.getTemperature());
        params.put(WEATHER_STATUS, weather.getWeatherStatus());
        params.put(UPDATE_DATE, weather.getUpdateDate());
        params.put(UPDATE_STATUS, weather.isUpdateStatus());
        params.put(COLLEAGUE_ID, colleagueId);
        return params;
    }
}
