package com.test.weather.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class WeatherRequest implements Serializable {

    private long weatherId;

    private String city;
}
