package com.test.weather.dto.converters;

import com.test.weather.dto.WeatherResponse;
import com.test.weather.entity.Weather;

import java.util.Date;

public class WeatherResponseToWeather {

    public static Weather convert(WeatherResponse response) {
        return Weather.builder()
                .temperature(response.getTemperature())
                .weatherStatus(response.getWeatherStatus())
                .updateStatus(true)
                .updateDate(new Date())
                .build();
    }
}
