package com.test.weather.dto.converters;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.test.weather.dto.WeatherResponse;

import java.io.IOException;

public class WeatherResponseDeserializer extends StdDeserializer<WeatherResponse> {

    public WeatherResponseDeserializer() {
        this(null);
    }

    public WeatherResponseDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public WeatherResponse deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        WeatherResponse weather = new WeatherResponse();
        weather.setWeatherStatus(node.get("weather").get(0).get("main").toString());
        weather.setTemperature(node.get("main").get("temp").intValue());
        return weather;
    }
}
