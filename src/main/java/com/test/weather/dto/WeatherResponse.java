package com.test.weather.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.test.weather.dto.converters.WeatherResponseDeserializer;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonDeserialize(using = WeatherResponseDeserializer.class)
public class WeatherResponse implements Serializable {

    private String weatherStatus;

    private int temperature;

}
