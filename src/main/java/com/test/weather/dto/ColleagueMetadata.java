package com.test.weather.dto;

import lombok.Data;

@Data
public class ColleagueMetadata {

    private long id;

    private String fullName;

    private String position;

    private String birthdayDate;

    private String city;
}
