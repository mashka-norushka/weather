package com.test.weather.exception;

import lombok.Getter;

@Getter
public class NotFoundException extends RuntimeException {

    private final Long id;
    private final String name;
    private final Class entityClass;

    public NotFoundException(Class entityClass, Long id) {
        super("Entity of class " + entityClass.getSimpleName() + " with id = " + id + " not found");
        this.entityClass = entityClass;
        this.id = id;
        this.name = null;
    }
}