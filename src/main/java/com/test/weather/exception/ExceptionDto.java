package com.test.weather.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collections;
import java.util.List;

@Getter
@AllArgsConstructor
public class ExceptionDto {

    private final List<String> errors;

    ExceptionDto(String message) {
        this(Collections.singletonList(message));
    }
}
