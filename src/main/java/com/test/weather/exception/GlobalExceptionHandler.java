package com.test.weather.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;
import java.util.List;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception ex) {
        log.error("Unhandled exception: ", ex);
        String message = ex.getMessage();
        StackTraceElement[] stackTrace = ex.getStackTrace();
        String stackTraceFirstLine = stackTrace.length > 0 ? stackTrace[0].getClassName() +
                " at " + stackTrace[0].getLineNumber() : "";
        if (message == null) {
            message = ex.getClass().getCanonicalName() + " " + stackTraceFirstLine;
        } else {
            message += " (" + stackTraceFirstLine + ")";
        }
        List<String> errors = Collections.singletonList(message);
        ExceptionDto errorDto = new ExceptionDto(errors);
        return new ResponseEntity<>(errorDto, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex) {
        List<String> errors = Collections.singletonList(ex.getMessage());
        ExceptionDto errorDto = new ExceptionDto(errors);
        return new ResponseEntity<>(errorDto, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}

