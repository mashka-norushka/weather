package com.test.weather.service;

import com.test.weather.entity.Colleague;
import com.test.weather.entity.Weather;
import com.test.weather.exception.NotFoundException;
import com.test.weather.repository.ColleagueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColleagueService {
    private ColleagueRepository repository;
    private WeatherService weatherService;

    @Autowired
    ColleagueService(ColleagueRepository repository, WeatherService weatherService) {
        this.repository = repository;
        this.weatherService = weatherService;
    }

    public Colleague findById(long id) {
        Colleague colleague = repository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(Colleague.class, id));
        Weather weather = weatherService.getWeatherByColId(id);
        colleague.setWeather(weather);
        return colleague;
    }

    public List<Colleague> getPaginated(int pageNum, int limit) {
        List<Colleague> colleagues = repository.getPaginated(pageNum, limit);
        colleagues.forEach(
                colleague -> colleague
                        .setWeather(weatherService
                                .getWeatherByColId(colleague.getId())));
        return colleagues;
    }

    public List<Colleague> getAll() {
        return repository.getAll();
    }

    public Colleague save(Colleague metadata) {
        Colleague colleague = repository.save(metadata);

        Weather weather = getWeather(metadata.getCity());
        Weather savedWeather = weatherService.save(colleague.getId(), weather);

        colleague.setWeather(savedWeather);
        return colleague;
    }

    public Colleague update(Colleague colleague) {
        Colleague updatedColleague = repository.update(colleague);
        Weather weather = weatherService.getWeatherByColId(colleague.getId());
        updatedColleague.setWeather(weather);
        return updatedColleague;
    }

    public void delete(long colleagueId) {
        repository.deleteById(colleagueId);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    private Weather getWeather(String city) {
        return weatherService.getWeatherByCity(city);
    }
}
