package com.test.weather.service;

import com.test.weather.client.WeatherClient;
import com.test.weather.dto.WeatherRequest;
import com.test.weather.dto.WeatherResponse;
import com.test.weather.dto.converters.WeatherResponseToWeather;
import com.test.weather.entity.Weather;
import com.test.weather.repository.WeatherRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class WeatherService {

    private WeatherRepository repository;
    private WeatherClient client;

    @Autowired
    public WeatherService(WeatherRepository repository, WeatherClient client) {
        this.repository = repository;
        this.client = client;
    }

    public void updateWeatherFromClient() {
        List<WeatherRequest> loosedUpdated = repository.getAllLoose();
        processUpdate(loosedUpdated);
        List<WeatherRequest> successUpdated = repository.getAllSuccess();
        processUpdate(successUpdated);
    }

    Weather save(long colleagueId, Weather weather) {
        return repository.create(colleagueId, weather);
    }

    Weather getWeatherByColId(long id) {
        return repository.getWeatherByColId(id);
    }

    Weather getWeatherByCity(String city) {
        Weather weather = null;
        try {
            WeatherResponse response = client.getWeatherByCity(city);
            weather = WeatherResponseToWeather.convert(response);
        } catch (Exception e) {
            weather = Weather.builder()
                    .updateDate(Date.from(Instant.now()))
                    .updateStatus(false)
                    .build();
        }
        return weather;
    }

    private void processUpdate(List<WeatherRequest> toProcess) {
        List<Weather> toUpdate = new ArrayList<>();
        for (WeatherRequest request : toProcess) {
            try {
                Weather weather = getWeatherByCity(request.getCity());
                weather.setId(request.getWeatherId());
                toUpdate.add(weather);
            } catch (Exception e) {
                log.info("Weather not update: " + e.getMessage());
                updateLoose(request);
            }
        }
        repository.updateAll(toUpdate);
    }

    private void updateLoose(WeatherRequest loosedUpdated) {
        Weather weather = new Weather();
        weather.setId(loosedUpdated.getWeatherId());
        weather.setUpdateStatus(false);
        weather.setUpdateDate(new Date());
        repository.updateDateAndStatus(weather);
    }
}
