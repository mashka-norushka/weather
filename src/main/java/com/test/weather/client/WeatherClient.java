package com.test.weather.client;

import com.test.weather.dto.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class WeatherClient {

    private RestTemplate restTemplate;

    @Value("${weather.api.key}")
    private String apiKey;

    @Autowired
    public WeatherClient(RestTemplateBuilder restTemplateBuilder) {
         restTemplate = restTemplateBuilder
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();
    }

    public WeatherResponse getWeatherByCity(String city) {
        ResponseEntity<WeatherResponse> response;
        response = restTemplate.getForEntity(createSinglUrl(city), WeatherResponse.class);
        return response.getBody();
    }

    public List<WeatherResponse> getListWeatherByCityId(Long... ids) {
        ResponseEntity<List<WeatherResponse>> response;
        response = restTemplate.exchange(createGroupUrl(ids), HttpMethod.GET, null,
                new ParameterizedTypeReference<List<WeatherResponse>>() {
                });
        return response.getBody();
    }

    private String createGroupUrl(Long... ids) {
        String urlFormat = "http://api.openweathermap.org/data/2.5/group?id=%s&appid=%s&units=metric";
        return String.format(urlFormat, Arrays.toString(ids), apiKey);
    }

    private String createSinglUrl(String city) {
        String urlFormat = "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&units=metric&lang=ru";
        return String.format(urlFormat, city, apiKey);
    }
}
