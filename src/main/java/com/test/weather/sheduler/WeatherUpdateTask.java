package com.test.weather.sheduler;

import com.test.weather.service.WeatherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class WeatherUpdateTask {

    private WeatherService weatherService;

    public WeatherUpdateTask(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @Scheduled(fixedRate = 30000)
    public void run() {
        weatherService.updateWeatherFromClient();
        log.info("scheduler run");
    }
}
