package com.test.weather;

import com.test.weather.entity.Colleague;
import com.test.weather.service.ColleagueService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ColleagueControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ColleagueService service;

    @AfterEach
    void deleteData() {
        service.deleteAll();
    }

    @Test
    void createColleague() throws Exception {
        String content = readFileAsString("createColleague.json");
        mockMvc.perform(post("/api/colleague")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("fullName", equalTo("Иванов Иван")))
                .andExpect(jsonPath("position", equalTo("devOps")))
                .andExpect(jsonPath("birthdayDate", equalTo("2000-01-02")))
                .andExpect(jsonPath("city", equalTo("city")))
                .andExpect(jsonPath("weather", any(Object.class)));
    }

    @Test
    void updateColleague() throws Exception {
        long saved = createColleagueData();
        String content = readFileAsString("updateColleague.json");
        String updated = content.replace("-1", String.valueOf(saved));
        mockMvc.perform(post("/api/colleague")
                .content(updated)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id", equalTo(saved)))
                .andExpect(jsonPath("fullName", equalTo("Иванов Иван")))
                .andExpect(jsonPath("position", equalTo("manager")))
                .andExpect(jsonPath("birthdayDate", equalTo("2000-01-02")))
                .andExpect(jsonPath("city", equalTo("London")));
    }

    @Test
    void getColleagueById() throws Exception {
        long saved = createColleagueData();
        mockMvc.perform(get("/api/colleague/" + saved)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("fullName", equalTo("test")))
                .andExpect(jsonPath("position", equalTo("test")))
                .andExpect(jsonPath("birthdayDate", equalTo("2000-01-02")))
                .andExpect(jsonPath("city", equalTo("London")));
    }

    @Test
    void deleteColleaueById() throws Exception {
        long saved = createColleagueData();
        mockMvc.perform(delete("/api/colleague/" + saved)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    long createColleagueData() throws ParseException {
        Colleague colleague = Colleague.builder()
                .fullName("test")
                .position("test")
                .city("London")
                .birthdayDate(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-02"))
                .build();
        Colleague saved = service.save(colleague);
        return saved.getId();
    }

    private String readFileAsString(String filename) throws IOException {
        File file = new File("src/test/resources/" + filename);
        Path path = file.toPath();
        byte[] fileBytes = Files.readAllBytes(path);
        return new String(fileBytes, StandardCharsets.UTF_8);
    }
}
